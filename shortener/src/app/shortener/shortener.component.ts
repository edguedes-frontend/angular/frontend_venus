import { Component, OnInit } from '@angular/core';
import { ShortenerService } from './shortener.service';
import { UrlLong } from './url-long';
import { Shortener } from './shortener';

@Component({
  selector: 'app-shortener',
  templateUrl: './shortener.component.html',
  styleUrls: ['./shortener.component.css']
})
export class ShortenerComponent implements OnInit {

  shortener: Shortener = {
    urlOriginal: "",
    token: "",
    countView: 0
  }
  urlLong: UrlLong = {
    url: ""
  };

  constructor(private shortenerService: ShortenerService) { }

  ngOnInit(): void {
  }

  submit() {
    this.shortenerService.submit(this.urlLong)
        .subscribe(
          (shortener) => {
            this.shortener = shortener
          },
          (err) => console.log(err)
        );
    //this.urlLong.url = "";
  }

  redirect() {
    this.shortenerService.goRedirect(this.shortener.token)
        .subscribe(
          (shortener) => {
            this.shortener = shortener
          },
          (err) => console.log(err)
        );
    window.open(this.shortener.urlOriginal);
  }

}
