export interface Shortener {
    urlOriginal: string;
    token: string;
    countView: number;
}
