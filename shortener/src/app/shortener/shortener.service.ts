import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Shortener } from './shortener';
import { Observable } from 'rxjs';
import { UrlLong } from './url-long';

@Injectable({
  providedIn: 'root'
})
export class ShortenerService {

  private readonly serverUrl = 'http://localhost:8082/api/v1';

  constructor(private http: HttpClient) { }

  submit(url: UrlLong): Observable<Shortener> {
    return this.http.post<Shortener>(this.serverUrl, url);
  }

  goRedirect(token: string): Observable<Shortener> {
    return this.http.get<Shortener>(`${this.serverUrl}/view/${token}`)
  }
}
